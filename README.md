# yyxbc

#### 介绍
阳阳学编程抖音号视频中的实验源代码
####  微信：yyxbc2010
项目说明：
#### esp8266FansClock
抖音粉丝灯牌，esp8266开发板接入oled显示屏和获取抖音粉丝个数，获赞数用显示屏显示。
源码链接::https://gitee.com/panzuji/yyxbc/tree/master/esp8266FansClock
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#### BlinkerIR  
红外万能遥控器1.0，  
源码链接::https://gitee.com/panzuji/yyxbc/tree/master/BlinkerIR  
esp8266+红外模块，实现遥控器功能  
功能描述： 
	1. 模拟家用电器的遥控器功能。 
	2. 接入blinker平台，通过手机可以远程控制。 
	3. 接入小爱同学、小度和天猫精灵，可以动动嘴就能控制你的家用电器。 
	4. 带红外学习功能，只要用遥控器的电器设备都可以接入。  
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#### blinker_NodeMcu_mi_duer_aligenie_IR_control
红外万能遥控器2.0，把家里的红外遥控器改成能用语音和手机app控制
	 1. 源码地址：
	链接:https://gitee.com/panzuji/yyxbc/tree/master/blinker_NodeMcu_mi_duer_aligenie_IR_control
	 2. 安装固件(nodemcu下载)
	链接: https://pan.baidu.com/s/14_uA8wqNHuUNtHblGA7Qqg 密码: e55w
	 3. 配套教程：
	链接:https://blog.csdn.net/panpanloveruth/article/details/115061976
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#### wificar
源码链接::https://gitee.com/panzuji/yyxbc/tree/master/wificar
nodemcu(esp8266-12e) + L298N 电机驱动板 ，wifi遥控小车，app inventor
开发的手机app wifi遥控器。

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#### blinker_esp8266-01s_mi_duer_aligenie_physicsSwitch
源码链接:https://gitee.com/panzuji/yyxbc/tree/master/blinker_esp8266-01s_mi_duer_aligenie_physicsSwitch
NodeMcu或esp-01/01s控制1路继电器，集成blinker平台，
用手机app控制1路继电器开关，添加一路物理开关控制，继电器用常开（NO）模式 应用实例：

    改造家里的开关成为智能开关，保留原有开关控制，零火版
    亮点：
        1.  成本不超20块，这可能是目前能找到的最便宜方案了吧。
        2.  体积较小，可以塞入86开关面板内。
        3.  手机app+智能音箱+物理开关控制，状态能同步。
        4.  接入点灯科技平台，支持wifi配网，包括blinker 设备密钥。
        5.  入手门槛大大降低，有电工基础，会接入线就行。
       
1. 固件有网页配网功能，固件烧写工具：  
链接:https://pan.baidu.com/s/1WsE_2gERyjaUZFbwgJnsOw 提取码:56r8 
2. 配套教程：
链接:https://blog.csdn.net/panpanloveruth/article/details/113977715
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
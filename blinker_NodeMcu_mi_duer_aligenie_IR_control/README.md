# yyxbc

#### 介绍
阳阳学编程抖音号视频中的实验源代码
####  微信：yyxbc2010
    项目说明：
    红外万能遥控器2.0，把家里的红外遥控器改成能用语音和手机app控制

    esp8266+红外模块，实现遥控器功能
    功能描述：
        1. 模拟家用电器的遥控器功能。
        2. 接入blinker平台，通过手机可以远程控制。
        3. 接入小爱同学、小度和天猫精灵，可以动动嘴就能控制你的家用电器。
        4. 带红外学习功能，只要用遥控器的电器设备都可以接入。

#### 硬件准备
### 硬件：
    1. 红外发射模块 *1
    2. 红外接收模块 1
    3. Nodemcu(826612f) 1
    4. 面包板1
    5. 杜邦线4根

#### 接线：
* 红外发射模块 | NodeMcu|
* IN | D2
* +5V |+5v
* GND |G

* 红外接入模块| NodeMcu
* DAT |D5
* VCC |+5v
* GND |G


1. 源码地址： 链接:https://gitee.com/panzuji/yyxbc/tree/master/blinker_NodeMcu_mi_duer_aligenie_IR_control 
2. 安装固件(nodemcu下载) 链接: https://pan.baidu.com/s/14_uA8wqNHuUNtHblGA7Qqg 密码: e55w
3. 配套教程： 链接:https://blog.csdn.net/panpanloveruth/article/details/115061976

获得具体连接电路图和更多资源，请参阅阳阳学编程网站 www.sooncore.com。  
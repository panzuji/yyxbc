#include <ESP8266WiFi.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>

//阳阳学编程wifi配置网demo，支持强制门户
//代码参考了https://blog.csdn.net/weixin_44220583/article/details/111562423
 
const char* AP_NAME = "Esp-阳阳学编程";//wifi名字
const byte DNS_PORT = 53;//DNS端口号
IPAddress apIP(192, 168, 4, 1);//esp8266-AP-IP地址
DNSServer dnsServer;//创建dnsServer实例
ESP8266WebServer server(80);//创建WebServer


char my_auth[32] = {0};
char my_ssid[32] = {0};
char my_password[64] = {0};

//配网页面代码
static const char HTML[] PROGMEM = R"KEWL(
<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset="utf-8">
  <title>阳阳学编程WIFI配网</title>
  <meta charset='UTF-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
</head>
<body>
  <form name='input' action='/' method='POST'>
        wifi名称: <br>
        <input type='text' value="panzujiMi10" name='ssid'><br>
        wifi密码:<br>
        <input type='text' value="moto1984" name='password'><br>
         点灯密钥:<br>
        <input type='text' value="a7a437131911" name='authkey'><br>
        <input type='submit' value='提交'>
   </form>
</body>
</html>)KEWL";


void handleRoot() {//访问主页回调函数
  server.send(200, "text/html", HTML);
}

void handleRootPost() {//Post回调函数
  Serial.println("handleRootPost");
  if (server.hasArg("ssid")) {//判断是否有账号参数
    Serial.print("got ssid:");
    strcpy(my_ssid, server.arg("ssid").c_str());//将账号参数拷贝到my_ssid中
    Serial.println(my_ssid);
  } else {//没有参数
    Serial.println("error, not found ssid");
    server.send(200, "text/html", "<meta charset='UTF-8'>error, not found ssid");//返回错误页面
    return;
  }
  if (server.hasArg("password")) {
    Serial.print("got password:");
    strcpy(my_password, server.arg("password").c_str());
    Serial.println(my_password);
  } else {
    Serial.println("error, not found password");
    server.send(200, "text/html", "<meta charset='UTF-8'>error, not found password");
    return;
  }

  if (server.hasArg("authkey")) {
    Serial.print("got authkey:");
    strcpy(my_auth, server.arg("authkey").c_str());
    Serial.println(my_auth);
  } else {
    Serial.println("error, not found authkey");
    server.send(200, "text/html", "<meta charset='UTF-8'>error, not found authkey");
    return;
  }
  
  server.send(200, "text/html", "<meta charset='UTF-8'>保存成功");//返回保存成功页面
  delay(2000);
  //连接wifi
  connectNewWifi();
}

void initBasic(void){//初始化基础
  Serial.begin(115200);
  WiFi.hostname("Smart-ESP8266");//设置ESP8266设备名
}

void initSoftAP(void){//初始化AP模式
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(apIP, apIP, IPAddress(255, 255, 255, 0));
  if(WiFi.softAP(AP_NAME)){
    Serial.println("ESP8266 SoftAP is right");
  }
}

void initWebServer(void){//初始化WebServer
  server.on("/", HTTP_GET, handleRoot);//设置主页回调函数
  server.onNotFound(handleRoot);//设置无法响应的http请求的回调函数
  server.on("/", HTTP_POST, handleRootPost);//设置Post请求回调函数
  server.begin();//启动WebServer
  Serial.println("WebServer started!");
}

void initDNS(void){//初始化DNS服务器
  if(dnsServer.start(DNS_PORT, "*", apIP)){//判断将所有地址映射到esp8266的ip上是否成功
    Serial.println("start dnsserver success.");
  }
  else Serial.println("start dnsserver failed.");
}

void connectNewWifi(void){
  WiFi.mode(WIFI_STA);//切换为STA模式
  WiFi.setAutoConnect(true);//设置自动连接
  WiFi.begin();
  Serial.println("");
  Serial.print("Connect to wifi");
  int count = 0;
   while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    count++;
    if(count > 10){//如果5秒内没有连上，就开启Web配网 可适当调整这个时间
      initSoftAP();
      initWebServer();
      initDNS();
      break;//跳出 防止无限初始化
    }
    Serial.print(".");
  }
  Serial.println("");
  if(WiFi.status() == WL_CONNECTED){//如果连接上 就输出IP信息 防止未连接上break后会误输出
    Serial.println("WIFI Connected!");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());//打印esp8266的IP地址
    server.stop();
  }

}

void setup() {
  initBasic();
  connectNewWifi();
}

void loop() {
  server.handleClient();
  dnsServer.processNextRequest();
}
